//-----------------------------------
//author：walter wong
//create time:2019-06-27
//description:语言基础类
//-----------------------------------

abstract class BaseLang{
  String get defaultTextTitle=>'';

  String get emptyContainer=>'';

  String get emptyValidatorErrorMsg=>'';
}